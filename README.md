# Runtime configuration files

This repository store a set of runtime confguration files for different
programs. To use them, it should be a simple matter of cloning this repository
to a suitable location and symbolically linking each file to the desired
location with

	ln -s /path/to/this/directory/rcfile /path/to/.link_name

This should make the file available to the program that requires it. For
example, ZSH expects its runtime file to be located in `$HOME/.zshrc`, and
therefore the `zshrc` file in this directory should be linked to with
`$HOME/.zshrc`.
