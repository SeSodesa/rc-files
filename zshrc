# Configure shell prompt
PS1='%B%F{%(!.red.green)}%n%F{blue}@%F{%(!.red.green)}%~%F{blue}
λ%F{white}%b '

# Set terminal tab width
if command -v tabs &> /dev/null
then
	tabs -4
else
	echo "The program tabs could not be found and hence tab width could not be
	set…"
fi

# ───────────────────────────────── Aliases ─────────────────────────────────

alias cd..="cd .."
